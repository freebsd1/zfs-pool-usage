#!/bin/sh

totaldisksize() {

vdevtype=$1
vdevgrpno=$2
poolname=$3

case "$vdevtype" in
      "mirror") mp_no=$(zpool list -v $poolname | grep 'multipath\|label\|da' | awk 'END{print NR}')
                mp_list=`zpool list -v $poolname | grep 'multipath\|label\|da' | awk '{print $1}'`
		vg_no=$(zpool list -v $poolname | grep $vdevtype | awk 'END{print NR}')
                tot_mp_size=0
                k=1
                while [ $k -le $mp_no ]; do
                  mptmplist=$(echo $mp_list | awk -v var="$k" '{print $var}')
                  mp_size=$(diskinfo -v $mptmplist | grep bytes | awk '{print $1}')
                  tot_mp_size=$(expr $tot_mp_size + $mp_size)
                  k=$(expr $k + 1)
                done

		if [ "$vg_no" -eq 1 ]
		then  
		  mp_first=$(zpool list -v $poolname | grep 'multipath\|label\|da' | awk 'NR==1{print $1}')
		  vg_size=$(diskinfo -v $mp_first | grep bytes | awk '{print $1}')
                  vg_disks=$(zpool list -v $poolname | grep 'multipath\|label\|da' | awk 'END{print NR}')
		else
		  mp_first=$(zpool list -v $poolname | grep 'multipath\|label\|da' | awk 'NR==1{print $1}')
                  vg_size=$(diskinfo -v $mp_first | grep bytes | awk '{print $1}')
		  vg_size=$(($vg_size * $vg_no))
		fi
      ;;
      "raidz1") mp_no=$(zpool list -v $poolname | grep 'multipath\|label\|da' | awk 'END{print NR}')
                mp_list=`zpool list -v $poolname | grep 'multipath\|label\|da' | awk '{print $1}'`
		vg_no=$(zpool list -v $poolname | grep $vdevtype | awk 'END{print NR}')
		tot_mp_size=0
                k=1
                while [ $k -le $mp_no ]; do
                  mptmplist=$(echo $mp_list | awk -v var="$k" '{print $var}')
                  mp_size=$(diskinfo -v $mptmplist | grep bytes | awk '{print $1}')
                  tot_mp_size=$(expr $tot_mp_size + $mp_size)
                  k=$(expr $k + 1)
                done
		
		if [ "$vg_no" -eq 1 ]
                then
                  mp_first=$(zpool list -v $poolname | grep 'multipath\|label\|da' | awk 'NR==1{print $1}')
                  vg_size=$(diskinfo -v $mp_first | grep bytes | awk '{print $1}')
                  vg_disks=$(zpool list -v $poolname | grep 'multipath\|label\|da' | awk 'END{print NR}')
		  vg_disks=$(expr $vg_disks - $vg_no)
                  vg_size=$(($vg_size * $vg_disks))
                else
                  mp_first=$(zpool list -v $poolname | grep 'multipath\|label\|da' | awk 'NR==1{print $1}')
                  vg_size=$(diskinfo -v $mp_first | grep bytes | awk '{print $1}')
                  vg_sl=$(zpool list -v $poolname | grep -n $vdevtype | cut -d : -f1 | awk 'NR==1{print $1}')
                  vg_el=$(zpool list -v $poolname | grep -n $vdevtype | cut -d : -f1 | awk 'NR==2{print $1}')
                  vg_disks=$(zpool list -v $poolname | awk -v var1="$vg_sl" -v var2="$vg_el" 'NR > var1 && NR < var2' | awk 'END{print NR}')
		  vg_disks=$(expr $vg_disks - 1)
		  vg_size=$(($vg_size * $vg_disks))
		  vg_size=$(($vg_size * $vg_no)) 
                fi
      ;;
esac

}

byteconversion() {

x=$1
y=$2

x1=${#x}
y1=${#y}

if [ "$x1" -le 12 ]
then
  rawsize=$(echo $x | awk '{print $1/1024/1024/1024 "GB"}')
  echo "$poolname Total Raw Size:" $rawsize
else
  rawsize=$(echo $x | awk '{print $1/1024/1024/1024/1024 "TB"}')
  echo "$poolname Total Raw Size:" $rawsize
fi

if [ "$y1" -le 12 ]
then
  usablesize=$(echo $y | awk '{print $1/1024/1024/1024 "GB"}')
  echo "$poolname Total Usable Size:" $usablesize
else
  usablesize=$(echo $y | awk '{print $1/1024/1024/1024/1024 "TB"}')
  echo "$poolname Total Usable Size:" $usablesize
fi

}

calcmetadata() {

poolname=$1

foundmeta=$(zpool status $poolname | grep -n meta | awk '{print $2}')
if [ "$foundmeta" = "meta" ]
then
	vdev_metanum=$(zpool status $poolname | grep -n meta | awk -F':' '{print $1}')
	meta_multipath=$(zpool status $poolname | awk -v var="$(expr $vdev_metanum + 1)" 'NR > var' | awk 'NR==1{print $1}')
	metamultinum=$(zpool list -v $poolname | grep -n $meta_multipath | awk -F':' '{print $1}')
	echo "$poolname Metadata Used:" $(zpool list -v $poolname | awk -v var="$(expr $metamultinum - 1)" NR==var | awk '{print $3}')
else
  x1=$(zpool list -H -o alloc $poolname)
  y1=$(zfs list -H -o used $poolname)

  case "${x1}${y1}" in
	*K*) meta1=$(zpool list -H -o alloc $poolname | awk -F 'M|G|T' '{print $1}')
	     if [ -z "${meta1##*K*}" ]; then
		meta1=$(echo $meta1 | tr -d K)
		meta1=$(expr $meta1 / 1024)
	     fi
	     meta2=$(zfs list -H -o used $poolname | awk -F 'M|G|T' '{print $1}')
	     if [ -z "${meta2##*K*}" ]; then
		meta2=$(echo $meta2 | tr -d K)
		meta2=$(expr $meta2 / 1024)
	     fi 
             metadata=`echo ${meta1} - ${meta2} |bc`
             echo "$poolname Metadata Used:" $metadata"K"
        ;;
	*M*) meta1=$(zpool list -H -o alloc $poolname | awk -F 'K|M|G|T' '{print $1}')
	     meta2=$(zfs list -H -o used $poolname | awk -F 'K|M|G|T' '{print $1}')
	     metadata=`echo ${meta1} - ${meta2} |bc`
	     echo "$poolname Metadata Used:" $metadata"M"
	;;
	*G*) meta1=$(zpool list -H -o alloc $poolname | awk -F 'K|M|G|T' '{print $1}')
	     meta2=$(zfs list -H -o used $poolname | awk -F 'K|M|G|T' '{print $1}')
	     metadata=`echo ${meta1} - ${meta2} |bc`
	     echo "$poolname Metadata Used:" $metadata"G"
	;;
	*T*) meta1=$(zpool list -H -o alloc $poolname | awk -F 'K|M|G|T' '{print $1}')
             meta2=$(zfs list -H -o used $poolname | awk -F 'K|M|G|T' '{print $1}')
             metadata=`echo ${meta1} - ${meta2} |bc`
             echo "$poolname Metadata Used:" $metadata"T"
  esac
fi
}

###Calculating number of pools and it's usage###

pool_count=$(zpool list -H -o name | awk 'END{print NR}')
pool_tot=$(expr $pool_count - 1)

echo -e "\n################################################"
echo -e "\nTotal Number of Pools Created: " $pool_tot

pool_name=`zpool list -H -o name | sed 's/zroot//'`
vdev_type=$(zpool list -v $pool_name | grep 'mirror\|raid' | awk 'NR==1{print $1}')
vdevgrp_no=$(zpool list -v $pool_name | grep 'mirror\|raid' | awk 'END{print NR}')

if [ "$pool_tot" = 1 ]
then

	echo -e "\nPool Name: "$pool_name

	echo -e "\n$pool_name Health: "$(zpool list -H -o health $pool_name)
	echo "$pool_name vdev Configured:" $vdev_type
	totaldisksize $vdev_type $vdevgrp_no $pool_name
	byteconversion $tot_mp_size $vg_size
	calcmetadata $pool_name
	echo "$pool_name Used Space: "$(zpool list -H -o alloc $pool_name)
	echo "$pool_name Free Space: "$(zpool list -H -o free $pool_name)
	echo "$pool_name Capacity Used: "$(zpool list -H -o cap $pool_name)
	echo "$pool_name Dedup Ratio: "$(zpool list -H -o dedupratio $pool_name)

	printf "\n"

else
        pool_namelist=`zpool list -H -o name | sed 's/zroot//'`
        echo -e "\nPools Name: " $pool_namelist

        i=1
        while [ $i -le $pool_tot ]; do
          varname=$(echo $pool_namelist | awk -v var="$i" '{print $var}')
	  vdevtype=$(zpool list -v $varname | grep 'mirror\|raid' | awk 'NR==1{print $1}')
          vdevgrpno=$(zpool list -v $varname | grep 'mirror\|raid' | awk 'END{print NR}')
          
	  echo -e "\n$varname Health: "$(zpool list -H -o health $varname)
	  echo "$varname vdev Configured:" $vdevtype
	  totaldisksize $vdevtype $vdevgrpno $varname
	  byteconversion $tot_mp_size $vg_size
	  calcmetadata $varname
          echo "$varname Used Space: "$(zpool list -H -o alloc $varname)
          echo "$varname Free Space: "$(zpool list -H -o free $varname)
          echo "$varname Capacity Used: "$(zpool list -H -o cap $varname)
	  echo "$varname Dedup Ratio: "$(zpool list -H -o dedupratio $varname)
	  i=$(expr $i + 1)
	  
	  printf "\n"
	done	

fi

echo "################################################"

###Calculating number of volumes and it's usage###

if [ "$pool_tot" = 1 ]
then

	#vol_count=$(zfs list -r -Ho mountpoint $pool_name | grep -v $pool_name | sed 's/^\///;s/\// /g' | awk 'END{print NR}')
	vol_count=$(zfs list -r -Ho mountpoint $pool_name | awk END'{print NR}')
	vol_count=$(expr $vol_count - 1)

	echo -e "\nTotal Number of Datasets in" $pool_name ":" $vol_count
	printf "\n"

	#vol_name=`zfs list -r -Ho mountpoint $pool_name | grep -v $pool_name | sed 's/^\///;s/\// /g'`
	vol_name=`zfs list -r -Ho mountpoint $pool_name | sed 's/^\///;s/\// /g' | awk '(NR > 1)''{print $2}'` 

	i=1
	while [ $i -le $vol_count ]; do
  	  volname=$(echo $vol_name | awk -v var="$i" '{print $var}')
  
  	  echo "$volname Used Space: "$(zfs list -r $pool_name | grep -w "$volname" | awk '{print $2}')
  	  echo "$volname Compression Ratio: "$(zfs get -H compressratio $pool_name | awk '{print $3}')
	  echo "$volname Deduplication: "$(zfs get -H dedup $pool_name | awk '{print $3}')
	  echo "$volname Available Space: "$(zfs list -r $pool_name | grep -w "$volname" | awk '{print $3}')
  	  i=$(expr $i + 1)
  	  printf "\n"
	done

	echo "################################################"

else
	pool_namelist=`zpool list -H -o name | sed 's/zroot//'`

	i=1
	
	while [ $i -le $pool_tot ]; do
	  varname=$(echo $pool_namelist | awk -v var="$i" '{print $var}')
	  #vol_count=$(zfs list -r -Ho mountpoint $varname | grep -v $varname | sed 's/^\///;s/\// /g' | awk 'END{print NR}')
	  vol_count=$(zfs list -r -Ho mountpoint $varname | awk END'{print NR}')
	  vol_count=$(expr $vol_count - 1)

	  echo -e "\nTotal Number of Datasets in" $varname ":" $vol_count
	  i=$(expr $i + 1)
          printf "\n"

	  #vol_name=`zfs list -r -Ho mountpoint $varname | grep -v $varname | sed 's/^\///;s/\// /g'
	  vol_name=$(zfs list -r -Ho mountpoint $varname | sed 's/^\///;s/\// /g' | awk '(NR > 1)''{print $2}')
	
	  j=1

	  while [ $j -le $vol_count ]; do
	    volname=$(echo $vol_name | awk -v var="$j" '{print $var}')

	    echo "$volname Used Space: "$(zfs list -r $varname | grep -w "$volname" | awk '{print $2}')
	    echo "$volname Compression Ratio: "$(zfs get -H compressratio $varname | awk '{print $3}')
	    echo "$volname Deduplication: "$(zfs get -H dedup $varname | awk '{print $3}')
            echo "$volname Available Space: "$(zfs list -r $varname | grep -w "$volname" | awk '{print $3}')
	    j=$(expr $j + 1)
	    printf "\n"
	  done
	done

	echo "################################################"
fi

###Calculating number of vdevs and it's usage###

if [ "$pool_tot" = 1 ]
then
	foundmeta=$(zpool status $pool_name | grep -n meta | awk '{print $2}')
	if [ "$foundmeta" = "meta" ]
	then
	    vdev_metanum=$(zpool status $pool_name | grep -n meta | awk -F':' '{print $1}')
	    vdev_meta=$(zpool status $pool_name | nawk -v var="$(expr $vdev_metanum + 1)" NR==var | awk -F' ' '{print $1}' | cut -d- -f1)
	    echo -e "\n"$foundmeta "device configured as:" $vdev_meta
	else
	    echo -e "\nMeta devices not provisioned"
	fi
	vdev_count=$(zpool list -v $pool_name | grep 'mirror\|raid' | awk 'END{print NR}')

	echo -e "\nvdev Configured in pool: "$(zpool list -v $pool_name | grep 'mirror\|raid' | awk 'NR==1{print $1}')
	echo "Total Number of vdev in" $pool_name "pool:" $vdev_count

	printf "\n"

	i=1
	while [ $i -le $vdev_count ]; do
	  echo $(zpool list -v $pool_name | grep 'mirror\|raid' | awk -v var="$i" 'NR==var{print $1}')  	  	  
  	  echo "vdev-"$i "Total Space: "$(zpool list -v $pool_name | grep 'mirror\|raid' | awk -v var="$i" 'NR==var{print $2}')
  	  echo "vdev-"$i "Used Space: "$(zpool list -v $pool_name | grep 'mirror\|raid' | awk -v var="$i" 'NR==var{print $3}')
          echo "vdev-"$i "Free Space: "$(zpool list -v $pool_name | grep 'mirror\|raid' | awk -v var="$i" 'NR==var{print $4}')
  	  i=$(expr $i + 1)
  	  printf "\n"
	done

	echo "################################################"

else
	pool_namelist=`zpool list -H -o name | sed 's/zroot//'`

	printf "\n"

	i=1
	while [ $i -le $pool_tot ]; do  
	  varname=$(echo $pool_namelist | awk -v var="$i" '{print $var}')
	  foundmeta=$(zpool status $varname | grep -n meta | awk '{print $2}')
          if [ "$foundmeta" = "meta" ]
          then
            vdev_metanum=$(zpool status $varname | grep -n meta | awk -F':' '{print $1}')
            vdev_meta=$(zpool status $varname | nawk -v var="$(expr $vdev_metanum + 1)" NR==var | awk -F' ' '{print $1}' | cut -d- -f1)
            echo -e "\n"$foundmeta "device configured as:" $vdev_meta
          else
            echo -e "\nMeta devices not provisioned"
          fi
	  vdev_count=$(zpool list -v $varname | grep 'mirror\|raid' | awk 'END{print NR}')
	  echo "vdev Configured in "$varname "is: "$(zpool list -v $varname | grep 'mirror\|raid' | awk 'NR==1{print $1}')
	  echo "Total Number of vdev in "$varname ":" $vdev_count
	  printf "\n"

	  j=1
	  while [ $j -le $vdev_count ]; do
	  echo $(zpool list -v $varname | grep 'mirror\|raid' | awk -v var="$j" 'NR==var{print $1}')
  	  echo "vdev-"$j "Total Space: "$(zpool list -v $varname | grep 'mirror\|raid' | awk -v var="$j" 'NR==var{print $2}')
  	  echo "vdev-"$j "Used Space: "$(zpool list -v $varname | grep 'mirror\|raid' | awk -v var="$j" 'NR==var{print $3}')
	  echo "vdev-"$j "Free Space: "$(zpool list -v $varname | grep 'mirror\|raid' | awk -v var="$j" 'NR==var{print $4}')
	  j=$(expr $j + 1)
          printf "\n"
	  done
	i=$(expr $i + 1)
	done

	echo "################################################"
fi

exit
