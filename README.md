# zfs pool usage

This shell script will provide the usage information about the pool and it's dataset. Execution of this script and its output is shown below,

In order to execute, #sh pool-volume_usage_details.sh

Output:

################################################

Total Number of Pools Created:  1

Pools Name:  stallion

stallion Health: ONLINE

stallion vdev Configured: raidz1

stallion Total Raw Size: 48GB

stallion Total Usable Size: 32GB

stallion Metadata Used: 4.23M

stallion Used Space: 5.73M

stallion Free Space: 47.0G

stallion Capacity Used: 0%

stallion Dedup Ratio: 1.00x

################################################

Total Number of Datasets in stallion : 3

vm1 Used Space: 117K

vm1 Compression Ratio: 1.00x

vm1 Deduplication: off

vm1 Available Space: 5.00G

vm2 Used Space: 117K

vm2 Compression Ratio: 1.00x

vm2 Deduplication: off

vm2 Available Space: 5.00G

vm3 Used Space: 117K

vm3 Compression Ratio: 1.00x

vm3 Deduplication: off

vm3 Available Space: 5.00G

################################################

Meta devices not provisioned

vdev Configured in stallion is: raidz1

Total Number of vdev in stallion : 2

raidz1

vdev-1 Total Space: 23.5G

vdev-1 Used Space: 2.95M

vdev-1 Free Space: 23.5G

raidz1

vdev-2 Total Space: 23.5G

vdev-2 Used Space: 2.78M

vdev-2 Free Space: 23.5G

################################################
